﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        /*
         * Static list of stocks, this will eventually be moved to your database
         * I can give you steps on how to move it to the database once you have all the logic and model complete
         * 
         * You can also create a viewmodel which would only have one property in this case, the list of stocks but focus on that later
         */

        // empty list, I will add 2 stocks to it
        List<Stock> Stocks = new List<Stock>();

        Stock s = new Stock { Id = 1, Code = "UFC", Prices = new double[] { 1.2, 2.4, 5.7, 6.3, 3, 5 } };
        Stock s2 = new Stock { Id = 2, Code = "BRB", Prices = new double[] { 1.5, 2.3, 5.5, 6.1, 3.4, 5.2 } };

        
        /*
         so at this point we have a model and a "view model" ready to go.
         we need to pass this into a view and the way we do that is through the controller action
         the action below has no model, it simply returns a view at this point - Index.

         public ActionResult Index()
          {
            return View();
          }
          */



        // now we pass in the stocks, so the view can render that if it wants to.
        // notice that the name of the action is Index(), it expects to go to a view thats names Index.html
        // so now go to index.html to see how we process this
        public ActionResult Index()
        {
            // here we add to the list
            // so before this point our view model is empty
            // in the future you can do this from the database
            // youll have a database context class, which will store a list of stocks
            // then youll do this -> ViewModel.Stocks = repository.FindAll();
            // repository here is the DAL, so it talks to the database
            // right now we can just add them and this is all in local memory


            Stocks.Add(s);
            Stocks.Add(s2);

            return View(Stocks);
        }


    }
}