﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{

    /*
     - ID, Code(3 chars), an array of prices for 30 days or 5 days etc
     

        Example:
        Stock s = new Stock();
        * this creates a blank object, 5 empty values in an array
        

        todo: add a stock option, use the datetime property, maybe create a 2 arrays
        example: 1 array is for prices, the other is for the dates.
                 pull information from both so it creates the dates and the prices in order
                 use 2 for loops for this. 
        
     */

    public class Stock
    {
        public int Id { get; set; }


        [StringLength(3, ErrorMessage = "Maximum number of characters for stock code is 3")]
        public string Code { get; set; }


        public double[] Prices { get; set; }

        public Stock()
        {
            Prices = new double[5];
        }
    }
}